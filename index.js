const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');

//Import Routes
const route = require('./src/routes/routes');
// const route = require('./src/routes/routes');

dotenv.config();

//Connect to DB
mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true, useFindAndModify: false }, () => {
  console.log('Connected to db!');
});

//Middleware
app.use(express.json());

//Route Middleware
app.use('/api', route);

app.listen(process.env.SERVER_PORT, () => {
  console.log(`Server is running on port ${process.env.SERVER_PORT}!`);
});
