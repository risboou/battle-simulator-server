const jwt = require('jsonwebtoken');
const BattleError = require('./battle.error');

const generateToken = data => {
  return jwt.sign(data, process.env.TOKEN_SECRET);
};

const verifyToken = accessToken => {
  try {
    return jwt.verify(accessToken, process.env.TOKEN_SECRET);
  } catch (error) {
    if (error) throw new BattleError(401, 'Invalid token!');
  }
};

module.exports = {
  generateToken,
  verifyToken
};
