const clientStatusEnum = {
  ACTIVE: 1,
  LEFT: 2,
  DESTROYED: 3
};

module.exports = {
  clientStatusEnum
};
