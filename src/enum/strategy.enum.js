const strategyEnum = {
  WEAKEST: 1,
  STRONGEST: 2,
  RANDOM: 3
};

module.exports = {
  strategyEnum
};
