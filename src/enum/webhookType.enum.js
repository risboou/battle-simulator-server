const webhookTypeEnum = {
  JOIN: 'army.join',
  LEAVE: 'army.leave',
  UPDATE: 'army.update'
};

module.exports = {
  webhookTypeEnum
};
