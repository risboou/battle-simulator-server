const WebHooks = require('node-webhooks');
const Client = require('../model/Client');
const { clientStatusEnum } = require('../enum/clientStatus.enum');
const { webhookTypeEnum } = require('../enum/webhookType.enum');
const webHooks = new WebHooks({
  db: {}
});
const emitter = webHooks.getEmitter();
emitter.on('*.success', function(name, statusCode, body) {
  console.log('Success on trigger webHook ' + name + ' with status code', statusCode, 'and body', body);
});

emitter.on('*.failure', function(shortname, statusCode, body) {
  removeWebhook(name);
  console.error('Error on trigger webHook ' + shortname);
});

const addWebhook = (name, api, savedUser, typeOfJoin) => {
  webHooks
    .add(name, api)
    .then(() => {
      armyJoin(name, savedUser, typeOfJoin);
    })
    .catch(err => {
      console.log(err);
    });
};

const removeWebhook = name => {
  webHooks
    .remove(name)
    .then(() => {
      Client.findOneAndUpdate({ accessToken: name }, { $set: { status: clientStatusEnum.LEFT } }, { new: true });
    })
    .catch(err => {
      console.error(err);
    });
};

const armyJoin = async (accessToken, savedUser, typeOfJoin) => {
  webHooks.trigger(accessToken, { message: 'You have successfully joined' });
  const clients = await Client.find({ status: clientStatusEnum.ACTIVE, accessToken: { $ne: accessToken } });
  const data = {
    type: webhookTypeEnum.JOIN,
    armyId: savedUser._id,
    squadsCount: savedUser.numberOfSquads,
    typeOfJoin
  };
  for (const client of clients) {
    webHooks.trigger(client.accessToken, { data });
  }
};

const armyLeave = async (accessToken, user) => {
  removeWebhook(accessToken);
  const data = {
    type: webhookTypeEnum.LEAVE,
    armyId: user._id,
    squadsCount: user.numberOfSquads
  };
  const clients = await Client.find({ status: clientStatusEnum.ACTIVE });
  for (const client of clients) {
    webHooks.trigger(client.accessToken, data);
  }
};

const armyUpdate = async (attack, type) => {
  const hpLeft = attack.receiverSquadsBefore - attack.damage > 0 ? attack.receiverSquadsBefore - attack.damage : 0;
  const message =
    'Attacker: ' +
    attack.attackerId +
    ', Receiver: ' +
    attack.receiverId +
    ', damage: ' +
    attack.damage +
    ', received half damage:  ' +
    attack.receivedHalfDamage +
    ', hp left: ' +
    hpLeft;
  if (type === 'destroy') {
    message = 'Client ' + attack.receiverId + ' is destroyed!';
  }
  const clients = await Client.find({ status: clientStatusEnum.ACTIVE });
  for (const client of clients) {
    webHooks.trigger(client.accessToken, { message: message });
  }
};

module.exports = {
  armyJoin,
  armyLeave,
  armyUpdate,
  addWebhook
};
