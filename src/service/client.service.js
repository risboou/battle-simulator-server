const { generateToken, verifyToken } = require('../../infrastructure/jwt');
const mongoose = require('mongoose');
const Client = require('../model/Client');
const BattleError = require('../../infrastructure/battle.error');
const { clientStatusEnum } = require('../enum/clientStatus.enum');
const { joinBattleValidation } = require('../validation/client.validation');
const { addWebhook, armyLeave } = require('../service/webhook');

const joinBattle = async (data, accessToken) => {
  try {
    const { error } = joinBattleValidation(data);
    if (error) throw new BattleError(400, error.details[0].message);
    let savedUser;
    let typeOfJoin = 'new';
    if (accessToken) {
      const verified = verifyToken(accessToken);
      savedUser = await Client.findOneAndUpdate(
        { _id: verified.userId, accessToken: accessToken },
        { $set: { numberOfSquads: data.numberOfSquads, status: clientStatusEnum.ACTIVE } },
        { new: true }
      );
      typeOfJoin = 'returned';
      if (!savedUser) throw new BattleError(400, 'Invalid token!');
    } else {
      data.userId = mongoose.Types.ObjectId();
      accessToken = generateToken({ userId: data.userId });
      savedUser = await createClient(data, accessToken);
    }
    addWebhook(accessToken, data.webhookUrl, savedUser, typeOfJoin);
    return savedUser;
  } catch (error) {
    throw new BattleError(error.code ? error.code : 500, error.message);
  }
};

const leaveBattle = async accessToken => {
  try {
    verifyToken(accessToken);
    const client = await Client.findOne({ accessToken: accessToken });
    armyLeave(accessToken, client);
    return client;
  } catch (error) {
    throw new BattleError(error.code ? error.code : 500, error.message);
  }
};

const createClient = async (data, accessToken) => {
  const client = new Client({
    _id: data.userId,
    name: data.name,
    numberOfSquads: data.numberOfSquads,
    webhookUrl: data.webhookUrl,
    status: clientStatusEnum.ACTIVE,
    accessToken: accessToken
  });
  return await client.save();
};

module.exports = {
  joinBattle,
  leaveBattle
};
