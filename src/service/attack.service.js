const Attack = require('../model/Attack');
const Client = require('../model/Client');
const { clientStatusEnum } = require('../enum/clientStatus.enum');
const BattleError = require('../../infrastructure/battle.error');
const { verifyToken } = require('../../infrastructure/jwt');
const { armyUpdate } = require('../service/webhook');

const attack = async (armyId, accessToken) => {
  try {
    const result = verifyToken(accessToken);
    const attacker = await Client.findById(result.userId);
    const receiver = await Client.findById(armyId);
    if (!receiver || receiver.status === clientStatusEnum.DESTROYED || receiver.status === clientStatusEnum.LEFT) {
      throw new BattleError(404, 'Client has left or destroyed!');
    }
    let numberOfAttack = 0;
    let damage = 0;
    let receivedHalfDamage = false;
    for (let i = 1; i <= attacker.numberOfSquads; i++) {
      if (Math.random() * 100 <= 100 / attacker.numberOfSquads) {
        numberOfAttack = i;
        damage = attacker.numberOfSquads / i;
        break;
      }
    }
    let receiverStatus = clientStatusEnum.ACTIVE;
    if (damage > 0) {
      if (Math.random() * 100 > receiver.numberOfSquads) {
        damage = damage / 2;
        receivedHalfDamage = true;
      }
      damage = Math.round(damage);
      const receiverNumberOfSquats = receiver.numberOfSquads - damage > 0 ? receiver.numberOfSquads - damage : 0;
      if (receiverNumberOfSquats < 1) {
        receiverStatus = clientStatusEnum.DESTROYED;
      }
      await Client.findByIdAndUpdate(
        receiver._id,
        { $set: { numberOfSquads: receiverNumberOfSquats, status: receiverStatus } },
        { new: true }
      );
    }
    const attack = await createAttack(
      attacker._id,
      receiver._id,
      attacker.numberOfSquads,
      receiver.numberOfSquads,
      damage,
      receivedHalfDamage,
      numberOfAttack
    );

    armyUpdate(attack, 'attack');
    if (receiverStatus === clientStatusEnum.DESTROYED) {
      armyUpdate(attack, 'destroy');
    }
    return attack;
  } catch (error) {
    throw new BattleError(error.code ? error.code : 500, error.message);
  }
};

const createAttack = async (attackerId, receiverId, attackerSquads, receiverSquadsBefore, damage, receivedHalfDamage, numberOfAttack) => {
  const attack = new Attack({
    attackerId,
    receiverId,
    attackerSquads,
    receiverSquadsBefore,
    damage,
    receivedHalfDamage,
    numberOfAttack
  });
  return await attack.save();
};

module.exports = {
  attack
};
