const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 6,
    max: 255
  },
  numberOfSquads: {
    type: Number,
    required: true,
    max: 100,
    min: 0
  },
  webhookUrl: {
    type: String,
    required: true,
    max: 1024,
    min: 6
  },
  status: {
    type: Number,
    required: true,
    max: 10,
    min: 1
  },
  accessToken: {
    type: String,
    required: true,
    max: 1024,
    min: 6
  },
  strategy: {
    type: Number,
    max: 3,
    min: 1
  }
});

module.exports = mongoose.model('Client', clientSchema);
