const mongoose = require('mongoose');

const attackSchema = new mongoose.Schema({
  attackerId: {
    type: String,
    required: true,
    min: 6,
    max: 1024
  },
  receiverId: {
    type: String,
    required: true,
    min: 6,
    max: 1024
  },
  attackerSquads: {
    type: Number,
    required: true,
    max: 100,
    min: 1
  },
  receiverSquadsBefore: {
    type: Number,
    required: true,
    max: 100,
    min: 0
  },
  damage: {
    type: Number,
    required: true,
    max: 100,
    min: 0
  },
  receivedHalfDamage: {
    type: Boolean,
    required: true
  },
  numberOfAttack: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('Attack', attackSchema);
