const router = require('express').Router();
const { joinBattle, leaveBattle } = require('../service/client.service');
const { attack } = require('../service/attack.service');

router.post('/join', async (req, res) => {
  try {
    const accessToken = req.query.accessToken;
    const result = await joinBattle(req.body, accessToken);
    res.status(200).send(result);
  } catch (error) {
    res.status(error.code).send(error.message);
  }
});

router.put('/attack/:armyId', async (req, res) => {
  try {
    const accessToken = req.query.accessToken;
    const armyId = req.params.armyId;
    const result = await attack(armyId, accessToken);
    res.status(200).send(result);
  } catch (error) {
    res.status(error.code).send(error.message);
  }
});

router.put('/leave', async (req, res) => {
  try {
    const accessToken = req.query.accessToken;
    const result = await leaveBattle(accessToken);
    res.status(200).send(result);
  } catch (error) {
    res.status(error.code).send(error.message);
  }
});
module.exports = router;
