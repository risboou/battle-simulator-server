const Joi = require('@hapi/joi');

const joinBattleValidation = data => {
  const schema = {
    name: Joi.string()
      .min(2)
      .required(),
    numberOfSquads: Joi.number()
      .min(10)
      .max(100)
      .required(),
    webhookUrl: Joi.string()
      .min(5)
      .required()
  };
  return Joi.validate(data, schema);
};

module.exports = {
  joinBattleValidation
};
